import firebase from 'firebase/app';
import 'firebase/firestore';
import React from 'react'

var firebaseConfig = {
    apiKey: "AIzaSyDqD3oBVHqhxdlcWKA_08otwgZOij-SH1U",
    authDomain: "spetrum-clone.firebaseapp.com",
    databaseURL: "https://spetrum-clone.firebaseio.com",
    projectId: "spetrum-clone",
    storageBucket: "spetrum-clone.appspot.com",
    messagingSenderId: "439365719207",
    appId: "1:439365719207:web:da96810b514117efcab782",
    measurementId: "G-C8RZ25CBCY"
  };
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.firestore();
export const AuthContext = React.createContext(null);
export default firebase;