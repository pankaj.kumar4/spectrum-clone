import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import NewPost from './components/NewPost';
import Messages from './components/Communities';
import Explore from './components/Explore';
import Profile from './components/Profile';
import './App.css';
import Communities from './components/Communities';
import Posts from './components/Posts';

function App() {
  return (
    <Router>
      <div className="App">
        <nav className="d-flex flex-column ">
          <Link
            to="/newpost"
            className="nav-items col-sm-12 "
            style={{ textDecoration: 'none' }}
          >
            New Post
          </Link>
          
          <Link
            to="/communities"
            className="nav-items col-sm-12 "
            style={{ textDecoration: 'none' }}
          >
            Communities
          </Link>
          <Link
            to="/explore"
            className="nav-items col-sm-12 "
            style={{ textDecoration: 'none' }}
          >
            Explore
          </Link>
          <Link
            to="/profile"
            className="nav-items col-sm-12 "
            style={{ textDecoration: 'none' }}
          >
            Profile
          </Link>
        </nav>
        <main>
          <Route path="/newpost" component={NewPost}></Route>
          <Route path="/communities" component={Communities}></Route>
          <Route path="/explore" component={Explore}></Route>
          <Route path="/profile" component={Profile}></Route>
        </main>

      </div>
    </Router>
  );
}

export default App;
