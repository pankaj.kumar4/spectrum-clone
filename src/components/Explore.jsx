import React, { Component } from 'react';
class Explore extends Component {
  state = {};
  render() {
    return (
      <div className="explore">
        <h2>Find a community</h2>
        <p>
          Try searching for topics like “crypto” or for products like “React”
        </p>
        <div className="searchBar text-dark rounded">
          <i class="fa fa-search" aria-hidden="true"></i>
          <input
            type="text"
            placeholder="Search for communities or topics..."
          />
        </div>
        <h2>Create your own community</h2>
        <p>Building communities on Spectrum is easy and free!</p>
        <button className="btn font-weight-bold text-dark rounded m-2">
          Create a community
        </button>
      </div>
    );
  }
}

export default Explore;
