import React, { Component } from 'react';
import Community from './community';
import Posts from './Posts';
import firebase from '../firebase.config';
class Messages extends Component {
  state = {
    showPost: false,
    activePost: [],
    activeId:'',
    communities: [
      {
        id: 1,
        name: 'Spectrum Support',
        desc:
          'Spectrum is a community platform for the future. This community is a great place to ask questions, request features, report bugs, and chat with the Spectrum team.',
        url: 'spectrum.chat',
        members: 8928,
        posts: [
          'this is Spectrum community',
          'new to community',
          'testing messages',
          'looking for tech'
        ]
      },
      {
        id: 2,
        name: 'ZEIT',
        desc:
          'Our mission is to make cloud computing as easy and accessible as mobile computing. You can find our Next.js community here.',
        url: 'https://zeit.co',
        members: 12880,
        posts: [
          'this is ZEIT',
          'new to community',
          'testing messages',
          'looking for tech'
        ]
      },
      {
        id: 3,
        name: 'CodeSandbox',
        desc:
          'Spectrum is a community platform for the future. This community is a great place to ask questions, request features, report bugs, and chat with the Spectrum team.',

        url: 'https://codesandbox.io',
        members: 3151,
        posts: [
          'this is CodeSandbox',
          'new to community',
          'testing messages',
          'looking for code'
        ]
      },
      {
        id: 4,
        name: 'CodePen',
        desc:
          'Hey Everybody! Come share Pens, ask front-end coding questions, and get/give feedback with your CodePen pals',
        url: 'https://codepen.io',
        members: 5132,
        posts: [
          'this is CodePen',
          'new to community',
          'testing messages',
          'looking for tech'
        ]
      }
    ]
  };
  updatePost=()=>{
   this.state.communities.forEach(community=>{
     if(community.id === this.state.activeId){
      this.setState({activePost:community.posts})
     }
   }) 
  }
  showPosts = postId => {
    this.state.communities.map(community => {
      if (community.id === postId) {
        this.setState({
          showPost: !this.state.showPost,
          activePost: community.posts,
          activeId:community.id
        });
      }
    });
    this.setState({ showPost: true });
  };
  componentDidMount() {
    this.fetchCommunities();
  }
  fetchCommunities = () => {
    firebase
      .firestore()
      .collection('Communities')
      .onSnapshot(snapshot => {
        let fetchedcommunities = [];
        snapshot.forEach(doc => {
          let data = doc.data();
          
          data.id = doc.id;
          fetchedcommunities.push(data);
        });
        this.setState({ communities: fetchedcommunities });
        this.updatePost()
      })
      
  };
  addNewPost = post => {
  };
  render() {
    console.log(this.state)
    return (
      <React.Fragment>
        <div className="communities ">
          {this.state.communities.map(community => (
            <Community community={community} showPosts={this.showPosts} />
          ))}
        </div>
        <div className="container">
          {this.state.showPost ? (
            <Posts posts={this.state.activePost} id={this.state.activeId} />
          ) : (
            'Please select a community'
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default Messages;
