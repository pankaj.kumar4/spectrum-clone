import React, { Component } from 'react';
import Posts from './Posts';
import { Link } from 'react-router-dom';
class Community extends Component {
  state = {
    messages: [
      'this is random message',
      'new to community',
      'testing messages',
      'looking for tech'
    ]
  };

  render() {
    return (
      <div className="community bg-primary rounded">
        <h3 className="font-weight-bold">{this.props.community.name}</h3>
        <h6> {this.props.community.desc}</h6>
        <h6> {this.props.community.url} </h6>
        <h6>{this.props.community.members} members</h6>

        <button
          className="btn btn-success"
          onClick={this.props.showPosts.bind(this, this.props.community.id)}
        >
          Posts
        </button>
      </div>
    );
  }
}

export default Community;
