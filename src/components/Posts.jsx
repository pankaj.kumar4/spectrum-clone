import React, { Component } from 'react';
import firebase from '../firebase.config'
class Posts extends Component {
  state = {
    newPost: ''
  };
  handleChange = e => {
    this.setState({ newPost: e.target.value });
  };
  updatePosts=()=>{
    let posts=[...this.props.posts,this.state.newPost]
    // console.log(posts)
    firebase.firestore().collection('Communities').doc(this.props.id).update({posts})
  }
  render() {
    return (
      <div className="messages">
        <input
          type="text"
          placeholder="Enter new post"
          onChange={this.handleChange}
        ></input>
            <button
            className="btn btn-danger"
            onClick={this.updatePosts}
            >
            Post
            </button>
        {this.props.posts.map(post => (
          <h6>{post}</h6>
        ))}
      </div>
    );
  }
}

export default Posts;
